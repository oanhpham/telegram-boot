package ttc.solution.chatbot_ttc.entities;


import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "message")
@Data
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "request")
    private String request;

    @Column(name = "response")
    private String response;

}