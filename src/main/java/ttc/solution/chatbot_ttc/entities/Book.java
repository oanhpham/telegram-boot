package ttc.solution.chatbot_ttc.entities;

import lombok.Data;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
public class Book {

    private Integer id;

    private String bookId;

    private String bookName;

    private boolean status;

    private Double price;

}
