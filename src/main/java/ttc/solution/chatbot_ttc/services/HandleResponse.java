package ttc.solution.chatbot_ttc.services;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;

public interface HandleResponse {

    SendMessage handle(Update update);

}
