package ttc.solution.chatbot_ttc.services;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Service
public class RetrieveBook {

    static final String URL_BOOKS = "http://localhost:8080/book/filter?book_id={bookId}&book_name={bookName}&price={price}&status={status}";
    static final String url = "http://localhost:8080/book/{id}";

  /*  public String retrieve(String bookId, String bookName, String price, String status) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("bookId", bookId);
        params.put("bookName", bookName);
        params.put("price", price);
        params.put("status", status);
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(URL_BOOKS, String.class, params);
    }*/

  public String retrieve(Integer id) {
      Map<String, Integer> params = new HashMap<String, Integer>();
      params.put("id", id);
      RestTemplate restTemplate = new RestTemplate();
      return restTemplate.getForObject(url, String.class, params);
  }
}
