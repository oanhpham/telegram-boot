package ttc.solution.chatbot_ttc.services;

import ttc.solution.chatbot_ttc.entities.Product;

import java.util.List;

public interface ProductService {

    List<Product> filterProduct (String productName, String productColor, String cost);
}
