package ttc.solution.chatbot_ttc.services.impl;

import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.User;
import ttc.solution.chatbot_ttc.services.HandleResponse;

@Service
public class HandleResponseGreetingImpl implements HandleResponse {
    @Override
    public SendMessage handle(Update update) {
        SendMessage response = new SendMessage();
        response.setChatId(update.getMessage().getChatId());
        User user = update.getMessage().getFrom();
        String text = user.getFirstName() +" "+ user.getLastName();
        response.setText("welcome "+text+ " to chat bot ttc");
        return response;
    }
}
