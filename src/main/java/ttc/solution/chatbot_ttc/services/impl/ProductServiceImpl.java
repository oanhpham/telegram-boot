package ttc.solution.chatbot_ttc.services.impl;

import org.springframework.stereotype.Service;
import ttc.solution.chatbot_ttc.entities.Product;
import ttc.solution.chatbot_ttc.services.ProductService;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Product> filterProduct(String productName, String productColor, String cost) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Product> cq = cb.createQuery(Product.class);

        Root<Product> product = cq.from(Product.class);
        List<Predicate> predicates = new ArrayList<>();
        if (!productName.isEmpty()) {
            predicates.add(cb.equal(product.get("productName"), productName));
        }
        if (!productColor.isEmpty()) {
            predicates.add(cb.equal(product.get("productColor"), productColor));
        }
        if (!cost.isEmpty()) {
            Double parseCost = Double.parseDouble(cost);
            predicates.add(cb.equal(product.get("cost"), parseCost));
        }
        cq.where(predicates.toArray(new Predicate[0]));

        return em.createQuery(cq).getResultList();
    }
}
