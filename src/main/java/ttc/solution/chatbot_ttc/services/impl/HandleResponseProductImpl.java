package ttc.solution.chatbot_ttc.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import ttc.solution.chatbot_ttc.entities.Product;
import ttc.solution.chatbot_ttc.repositories.ProductRepository;
import ttc.solution.chatbot_ttc.services.HandleResponse;

import java.util.List;

@Service
public class HandleResponseProductImpl implements HandleResponse {

    @Autowired
    ProductRepository repository;

    @Override
    public SendMessage handle(Update update) {
        String[] params = update.getMessage().getText().split("/");
        List<Product> products = repository.findProductByCost(Double.parseDouble(params[1]));
        SendMessage response = new SendMessage();
        response.setChatId(update.getMessage().getChatId());
        StringBuilder text = new StringBuilder();
        if (products.size() > 0)
            for (Product it : products) {

                String product = "\n" + it.getProductName() + "\t" + it.getProductColor() + "\t" + it.getCost();
                text.append(product);
            }
        else {
            text.append("have'nt any items you want");
        }
        response.setText(text.toString());

        return response;
    }
}
