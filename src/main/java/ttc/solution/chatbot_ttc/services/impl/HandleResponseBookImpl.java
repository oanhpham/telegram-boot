package ttc.solution.chatbot_ttc.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import ttc.solution.chatbot_ttc.services.HandleResponse;
import ttc.solution.chatbot_ttc.services.RetrieveBook;

@Service
public class HandleResponseBookImpl implements HandleResponse {

    @Autowired
    RetrieveBook retrieveBook;

    @Override
    public SendMessage handle(Update update) {
        String text = update.getMessage().getText();
        String[] params = text.split("/");
        String books = retrieveBook.retrieve(Integer.parseInt(params[1]));
        SendMessage response = new SendMessage();
        if (books != null) {
            response.setChatId(update.getMessage().getChatId());
        } else {
            books = "have'nt any item as you want";
        }
        response.setText(books);
        return response;
    }
}
