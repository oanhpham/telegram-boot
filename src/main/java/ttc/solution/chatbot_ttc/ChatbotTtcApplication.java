package ttc.solution.chatbot_ttc;

import org.apache.log4j.BasicConfigurator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class ChatbotTtcApplication {
    public static void main(String[] args) {
        BasicConfigurator.configure();
        SpringApplication.run(ChatbotTtcApplication.class, args);
    }

}
