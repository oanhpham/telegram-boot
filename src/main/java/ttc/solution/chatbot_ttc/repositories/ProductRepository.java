package ttc.solution.chatbot_ttc.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ttc.solution.chatbot_ttc.entities.Product;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Integer> {

    List<Product> findProductByCost(Double cost);
}
