package ttc.solution.chatbot_ttc.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ttc.solution.chatbot_ttc.entities.Message;

public interface MessageRepository extends JpaRepository<Message, Integer> {
}
