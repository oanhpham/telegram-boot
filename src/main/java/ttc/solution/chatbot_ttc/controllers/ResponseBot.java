package ttc.solution.chatbot_ttc.controllers;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ttc.solution.chatbot_ttc.services.impl.HandleResponseBookImpl;
import ttc.solution.chatbot_ttc.services.impl.HandleResponseGreetingImpl;
import ttc.solution.chatbot_ttc.services.impl.HandleResponseProductImpl;

import javax.annotation.PostConstruct;

@Component
public class ResponseBot extends TelegramLongPollingBot {

    private static final Logger logger = Logger.getLogger(ResponseBot.class);

    public static final String GREETING ="hello hi halo bojour xin chao";

    public static final String INTERNAL_REQUEST = "search";

    public static final String EXTERNAL_REQUEST = "filter";



    @Autowired
    HandleResponseGreetingImpl handleResponseGreeting;

    @Autowired
    HandleResponseProductImpl handleResponseProduct;

    @Autowired
    HandleResponseBookImpl handleResponseBook;

    @Value("${bot.token}")
    private String token;

    @Value("${bot.username}")
    private String username;

    @Override
    public String getBotToken() {
        return token;
    }

    @Override
    public String getBotUsername() {
        return username;
    }

    @Override
    public void onUpdateReceived(Update update) {
        if (update.hasMessage()) {
            SendMessage response = new SendMessage();
            String text = update.getMessage().getText().toLowerCase();
            if (GREETING.contains(text)) {
                response = handleResponseGreeting.handle(update);
            }

            if (text.contains(INTERNAL_REQUEST)) {
                response = handleResponseProduct.handle(update);
            }

            if (text.contains(EXTERNAL_REQUEST)) {
                response = handleResponseBook.handle(update);
            }

            try {
                execute(response);
                logger.info("Get message" +text+ "from" +update.getMessage().getFrom());
            } catch (TelegramApiException e) {
                logger.error("Failed to send message"+text+ " due to error: "+ e.getMessage());
            }
        }
    }

    @PostConstruct
    public void start() {
        logger.info("username: "+username+ " token: "+ token);
    }
}

